CREATE DATABASE stats;

use stats;

CREATE TABLE stats.snap (
  snap_id int(1) NOT NULL DEFAULT '0',
  dttm datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (snap_id));

CREATE TABLE stats.snap_innodb_locks (
  snap_id int(11) DEFAULT NULL,
  dttm datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  lock_id varchar(81) NOT NULL DEFAULT '',
  lock_trx_id varchar(18) NOT NULL DEFAULT '',
  lock_mode varchar(32) NOT NULL DEFAULT '',
  lock_type varchar(32) NOT NULL DEFAULT '',
  lock_table varchar(1024) NOT NULL DEFAULT '',
  lock_index varchar(1024) DEFAULT NULL,
  lock_space varchar(1024) DEFAULT NULL,
  lock_page bigint(21) unsigned DEFAULT NULL,
  lock_rec bigint(21) unsigned DEFAULT NULL,
  lock_data varchar(8192) DEFAULT NULL,
  KEY snap_innodb_locks_snap_id (snap_id),
  CONSTRAINT snap_innodb_locks_snap_id FOREIGN KEY (snap_id) REFERENCES snap (snap_id) ON DELETE CASCADE);  

CREATE TABLE stats.snap_tables (
  snap_id int(11) DEFAULT NULL,
  dttm datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  TABLE_SCHEMA varchar(64) NOT NULL DEFAULT '',
  TABLE_NAME varchar(64) NOT NULL DEFAULT '',
  ENGINE varchar(64) DEFAULT NULL,
  TABLE_ROWS bigint(21) unsigned DEFAULT NULL,
  DATA_LENGTH bigint(21) unsigned DEFAULT NULL,
  INDEX_LENGTH bigint(21) unsigned DEFAULT NULL,
  DATA_FREE bigint(21) unsigned DEFAULT NULL,
  KEY tables_snap_id (snap_id),
  CONSTRAINT tables_snap_id FOREIGN KEY (snap_id) REFERENCES snap (snap_id) ON DELETE CASCADE);


CREATE TABLE stats.snap_processlist (
  snap_id int(11) DEFAULT NULL,
  dttm datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  DB varchar(64) DEFAULT NULL,
  COMMAND varchar(16) NOT NULL DEFAULT '',
  STATE varchar(64) DEFAULT NULL,
  KEY processlist_snap_id (snap_id),
  CONSTRAINT processlist_snap_id FOREIGN KEY (snap_id) REFERENCES snap (snap_id) ON DELETE CASCADE);  

